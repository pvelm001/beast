/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor
import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}

import java.awt.geom.AffineTransform

class FakeRaster(numTilesX: Int, numTilesY: Int, tileWidth: Int, tileHeight: Int) extends IRasterReader {
  override lazy val metadata: RasterMetadata = new RasterMetadata(0, 0,
    numTilesX * tileWidth, numTilesY * tileHeight, tileWidth, tileHeight,
    0, AffineTransform.getScaleInstance(1.0, 1.0))

  override def readTile(tileID: Int): ITile = null

  override def getTileOffset(tileID: Int): Long = 0

  override def close(): Unit = None
}
