package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.common.BeastOptions
import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}
import edu.ucr.cs.bdlab.beast.io.tiff.TiffConstants
import org.apache.hadoop.fs.Path
import org.apache.spark.beast.CRSServer
import org.apache.spark.rdd.RDD
import org.apache.spark.test.ScalaSparkTest
import org.geotools.referencing.CRS
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import java.awt.geom.AffineTransform
import java.io.File

@RunWith(classOf[JUnitRunner])
class GeoTiffWriterTest extends FunSuite with ScalaSparkTest {

  test("Write simple file with one tile") {
    val affineTransform = new AffineTransform()
    affineTransform.translate(2.0, 2.0)
    affineTransform.scale(3.0, 1.0)
    val metadata = new RasterMetadata(0, 0, 100, 100, 100, 100, 4326,
      affineTransform)
    val pixelValues = new Array[Int](100 * 100)
    pixelValues(10) = 123
    pixelValues(100) = 125
    pixelValues(350) = 44
    val originalTile = new InMemoryTile(0, metadata, pixelValues)

    val geotTiffPath = new Path(scratchPath, "temp.tif")
    val fileSystem = geotTiffPath.getFileSystem(sparkContext.hadoopConfiguration)

    val geoTiffWriter = new GeoTiffWriter(geotTiffPath, metadata, false, new BeastOptions())
    geoTiffWriter.write(originalTile)
    geoTiffWriter.close()

    // Read it back
    val geoTiffReader = new GeoTiffReader()
    geoTiffReader.initialize(fileSystem, geotTiffPath.toString, "0", new BeastOptions, sparkContext.getConf)
    assertResult(4326)(geoTiffReader.metadata.srid)
    assertResult(metadata.g2m)(geoTiffReader.metadata.g2m)
    val actualTile = geoTiffReader.readTile(0)
    assertResult(originalTile.getPixelValueAs[Int](10, 0))(actualTile.getPixelValueAs[Int](10, 0))
    assertResult(originalTile.getPixelValueAs[Int](0, 1))(actualTile.getPixelValueAs[Int](0, 1))
    assertResult(originalTile.getPixelValueAs[Int](50, 3))(actualTile.getPixelValueAs[Int](50, 3))
    geoTiffReader.close()
  }

  test("Write simple file with LZW encoder") {
    val affineTransform = new AffineTransform()
    affineTransform.translate(2.0, 2.0)
    affineTransform.scale(3.0, 1.0)
    val metadata = new RasterMetadata(0, 0, 100, 100, 100, 100, 4326,
      affineTransform)
    val pixelValues = new Array[Int](100 * 100)
    pixelValues(10) = 123
    pixelValues(100) = 125
    pixelValues(350) = 44
    val originalTile = new InMemoryTile(0, metadata, pixelValues)

    val geotTiffPath = new Path(scratchPath, "temp.tif")
    val fileSystem = geotTiffPath.getFileSystem(sparkContext.hadoopConfiguration)

    val geoTiffWriter = new GeoTiffWriter(geotTiffPath, metadata, false,
      GeoTiffWriter.Compression -> TiffConstants.COMPRESSION_LZW)
    geoTiffWriter.write(originalTile)
    geoTiffWriter.close()

    // Read it back
    val geoTiffReader = new GeoTiffReader()
    geoTiffReader.initialize(fileSystem, geotTiffPath.toString, "0", new BeastOptions, sparkContext.getConf)
    val actualTile = geoTiffReader.readTile(0)
    assertResult(originalTile.getPixelValueAs[Int](10, 0))(actualTile.getPixelValueAs[Int](10, 0))
    assertResult(originalTile.getPixelValueAs[Int](0, 1))(actualTile.getPixelValueAs[Int](0, 1))
    assertResult(originalTile.getPixelValueAs[Int](50, 3))(actualTile.getPixelValueAs[Int](50, 3))
    geoTiffReader.close()
  }

  test("Write simple file with multiple tile") {
    val crs = CRS.decode("EPSG:32640")
    val srid = CRSServer.crsToSRID(crs, sparkContext.getConf)
    val metadata = new RasterMetadata(0, 0, 100, 100, 10, 10, srid,
      new AffineTransform())
    var pixelValues = new Array[Int](10 * 10)
    pixelValues(10) = 123
    pixelValues(50) = 125
    pixelValues(72) = 44
    val originalTile1 = new InMemoryTile(0, metadata, pixelValues)

    pixelValues = new Array[Int](10 * 10)
    pixelValues(10) = 12
    pixelValues(50) = 251
    pixelValues(72) = 33
    val originalTile2 = new InMemoryTile(3, metadata, pixelValues)

    val geotTiffPath = new Path(scratchPath, "temp.tif")
    val fileSystem = geotTiffPath.getFileSystem(sparkContext.hadoopConfiguration)

    val geoTiffWriter = new GeoTiffWriter(geotTiffPath, metadata, false, new BeastOptions())
    geoTiffWriter.write(originalTile1)
    geoTiffWriter.write(originalTile2)
    geoTiffWriter.close()

    // Read it back
    val geoTiffReader = new GeoTiffReader()
    geoTiffReader.initialize(fileSystem, geotTiffPath.toString, "0", new BeastOptions, sparkContext.getConf)
    assertResult(metadata.srid)(geoTiffReader.metadata.srid)
    val actualTile1 = geoTiffReader.readTile(0)
    assertResult(originalTile1.getPixelValueAs[Int](0, 1))(actualTile1.getPixelValueAs[Int](0, 1))
    assertResult(originalTile1.getPixelValueAs[Int](0, 5))(actualTile1.getPixelValueAs[Int](0, 5))
    assertResult(originalTile1.getPixelValueAs[Int](2, 7))(actualTile1.getPixelValueAs[Int](2, 7))
    val actualTile2 = geoTiffReader.readTile(3)
    assertResult(originalTile2.getPixelValueAs[Int](30, 1))(actualTile2.getPixelValueAs[Int](30, 1))
    assertResult(originalTile2.getPixelValueAs[Int](30, 5))(actualTile2.getPixelValueAs[Int](30, 5))
    assertResult(originalTile2.getPixelValueAs[Int](32, 7))(actualTile2.getPixelValueAs[Int](32, 7))
    geoTiffReader.close()
  }

  test("Write RDD to GeoTiff") {
    val rasterFile = makeResourceCopy("/raptor/glc2000_small.tif")
    val rasterRDD = new RasterFileRDD(sparkContext, rasterFile.getPath, IRasterReader.RasterLayerID -> 0)
    val outputFile = new File(scratchDir, "glc.tif")
    GeoTiffWriter.saveAsGeoTiff(rasterRDD, outputFile.getPath, GeoTiffWriter.BitsPerSample -> 8)
    val readRaster = new RasterFileRDD(sparkContext, outputFile.getPath, IRasterReader.RasterLayerID -> 0)
    assertResult(rasterRDD.count())(readRaster.count())
  }

  test("Write partitioned RDD to GeoTiff") {
    val rasterFile = makeResourceCopy("/raptor/glc2000_small.tif")
    val rasterRDD = new RasterFileRDD(sparkContext, rasterFile.getPath, IRasterReader.RasterLayerID -> 0)
      .repartition(3)
    val outputFile = new File(scratchDir, "glc.tif")
    GeoTiffWriter.saveAsGeoTiff(rasterRDD, outputFile.getPath, GeoTiffWriter.BitsPerSample -> 8)
    val readRaster = new RasterFileRDD(sparkContext, outputFile.getPath, IRasterReader.RasterLayerID -> 0)
    assertResult(rasterRDD.count())(readRaster.count())
    assertResult(3)(readRaster.getNumPartitions)
  }

  test("Write partitioned RDD to GeoTiff in compatibility mode") {
    val rasterFile = makeResourceCopy("/raptor/glc2000_small.tif")
    val rasterRDD = new RasterFileRDD(sparkContext, rasterFile.getPath, IRasterReader.RasterLayerID -> 0)
      .repartition(3)
    val outputFile = new File(scratchDir, "glc.tif")
    GeoTiffWriter.saveAsGeoTiff(rasterRDD, outputFile.getPath,
      Seq(GeoTiffWriter.BitsPerSample -> 8, GeoTiffWriter.WriteMode -> "compatibility"))
    val readRaster = new RasterFileRDD(sparkContext, outputFile.getPath, IRasterReader.RasterLayerID -> 0)
    assertResult(rasterRDD.count())(readRaster.count())
    assertResult(1)(readRaster.getNumPartitions)
  }

  test("Write partial file in compatibility mode") {
    val metadata = new RasterMetadata(0, 0, 100, 100, 10, 10, 4326,
      new AffineTransform())
    var pixelValues = new Array[Int](10 * 10)
    pixelValues(10) = 123
    pixelValues(50) = 125
    pixelValues(72) = 44
    val originalTile1 = new InMemoryTile(0, metadata, pixelValues)

    pixelValues = new Array[Int](10 * 10)
    pixelValues(10) = 12
    pixelValues(50) = 251
    pixelValues(72) = 33
    val originalTile2 = new InMemoryTile(3, metadata, pixelValues)

    val geotTiffPath = new Path(scratchPath, "temp.tif")
    val rasterRDD: RDD[ITile] = sparkContext.parallelize(Seq(originalTile1, originalTile2))
    GeoTiffWriter.saveAsGeoTiff(rasterRDD, geotTiffPath.toString, Seq(GeoTiffWriter.WriteMode -> "compatibility"))

    // Read it back
    val readRaster = new RasterFileRDD(sparkContext, geotTiffPath.toString, IRasterReader.RasterLayerID -> 0)
    assertResult(2)(readRaster.count())
    assertResult(1)(readRaster.getNumPartitions)
  }

  test("Write GeoTIFF with Sinusoidal projection") {
    val crs = HDF4Reader.SinusoidalCRS
    val srid = CRSServer.crsToSRID(crs, sparkContext.getConf)
    val metadata = new RasterMetadata(0, 0, 100, 100, 100, 100, srid,
      new AffineTransform())
    val pixelValues = new Array[Int](100 * 100)
    pixelValues(10) = 123
    pixelValues(100) = 125
    pixelValues(350) = 44
    val originalTile = new InMemoryTile(0, metadata, pixelValues)

    val geotTiffPath = new Path(scratchPath, "temp.tif")
    val fileSystem = geotTiffPath.getFileSystem(sparkContext.hadoopConfiguration)

    val geoTiffWriter = new GeoTiffWriter(geotTiffPath, metadata, false, new BeastOptions())
    geoTiffWriter.write(originalTile)
    geoTiffWriter.close()

    // Read it back
    val geoTiffReader = new GeoTiffReader()
    geoTiffReader.initialize(fileSystem, geotTiffPath.toString, "0", new BeastOptions, sparkContext.getConf)
    val parsedCRS = CRSServer.sridToCRS(geoTiffReader.metadata.srid, sparkContext.getConf)
    //assertResult(crs)(parsedCRS) // Ideally, this line should work but it does not
    assert(parsedCRS.getName.getCode.toLowerCase().contains("sinusoidal"))
    geoTiffReader.close()
  }

}
